import { expect } from 'chai'
import {
  mount,
  createLocalVue
} from '@vue/test-utils'
import UserProfileComponent from '@/components/UserProfileComponent.vue'
import User from '@/models/User'

import Vuetify from 'vuetify'
import Vuex from 'vuex'
import Faker from 'faker'

const localVue = createLocalVue()

localVue.use(Vuetify)
localVue.use(Vuex)

describe('UserProfileComponent.vue', () => {
  let getters
  let store

  beforeEach(() => {
    let user = new User({
      id: 1,
      name: Faker.name.findName(),
      email: Faker.internet.email(),
      level: 0,
      goals: JSON.stringify({
        future: Faker.lorem.sentence(),
        long: Faker.lorem.sentence(),
        short: Faker.lorem.sentence()
      })
    })
    getters = { profile: () => user }

    store = new Vuex.Store({ getters })
  })

  it('renders user name', () => {
    const wrapper = mount(UserProfileComponent, {
      localVue,
      store
    })
    expect(wrapper.text()).to.include(getters.profile().name)
  })
  it('renders user email', () => {
    const wrapper = mount(UserProfileComponent, {
      localVue,
      store
    })
    expect(wrapper.text()).to.include('Profile', 'Missing "Profile" header')
    expect(wrapper.text()).to.include('Email', 'Missing "Email" label')
    expect(wrapper.text()).to.include(getters.profile().email)
  })
  it('renders user level as text', () => {
    const wrapper = mount(UserProfileComponent, {
      // propsData: { title: msg },
      localVue,
      store
    })
    expect(wrapper.text()).to.include('Level', 'Missing "Level" label')
    expect(wrapper.text()).to.include('Admin')
  })
  it('renders user goals', () => {
    const wrapper = mount(UserProfileComponent, {
      localVue,
      store
    })
    expect(wrapper.text()).to.include('Goals', 'Missing "Goals" header')
    expect(wrapper.text()).to.include('Future', 'Missing "Future" label')
    expect(wrapper.text()).to.include(getters.profile().goals.future, 'Missing future goals')
    expect(wrapper.text()).to.include('Long Term', 'Missing "Long Term" header')
    expect(wrapper.text()).to.include(getters.profile().goals.long, 'Missing long term goals')
    expect(wrapper.text()).to.include('Short Term', 'Missing "Short Term" header')
    expect(wrapper.text()).to.include(getters.profile().goals.short, 'Missing short term goals')
  })
})

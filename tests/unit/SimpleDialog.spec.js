import { expect } from 'chai'
import {
  mount,
  createLocalVue,
  config
} from '@vue/test-utils'
import SimpleDialog from '@/components/dialogs/SimpleDialog'

import addDataApp from '../util/AddDataApp'

import Vuetify from 'vuetify'

// Fixes Vuetify complaints about missing [data-app]
addDataApp()

describe('SimpleDialog.vue', () => {
  let wrapper, activator, dialog, textbox, done, cancel, label
  beforeEach(() => {
    const localVue = createLocalVue()
    localVue.use(Vuetify)

    wrapper = mount(SimpleDialog, {
      localVue,
      slots: { default: ['<button name="test"/>'] },
      propsData: {
        name: 'Name',
        value: 'Test',
        rules: [v => !!v || 'Required']
      }
    })

    config.silent = true
    localVue.config.silent = true

    activator = wrapper.find('button[name="test"]')

    dialog = wrapper.find('div.dialog')

    label = dialog.find('label')
    textbox = dialog.find('input')

    done = dialog.find('button.success--text')

    cancel = dialog.find('button.warning--text')

    expect(activator.is('button')).to.be.true
    expect(label.is('label')).to.be.true
    expect(textbox.is('input')).to.be.true
    expect(done.is('button')).to.be.true
    expect(cancel.is('button')).to.be.true
  })

  // Helpers
  function openDialog () {
    // Assume
    expect(dialog.isVisible()).to.be.false
    expect(wrapper.vm.$data.dialog).to.be.false

    // Act
    activator.trigger('click')

    // Assert
    expect(dialog.isVisible(), 'dialog is not visible').to.be.true
    expect(wrapper.vm.$data.dialog, 'data.dialog is false').to.be.true
  }

  function expectClosed () {
    expect(dialog.isVisible(), 'dialog is still visible').to.be.false
    expect(wrapper.vm.$data.dialog, 'data.dialog is still true').to.be.false
  }

  it('opens the dialog when activator is clicked', () => {
    openDialog()
  })

  it('has a textbox with the correct value', () => {
    // Assume
    expect(wrapper.props().value).to.equal('Test')

    // Act
    openDialog()

    // Assert
    expect(textbox.element.value).to.equal('Test')
  })

  it('closes dialog when clicking cancel', () => {
    // Assume
    openDialog()
    let text = 'New Test'

    // Act
    textbox.setValue(text)

    cancel.trigger('click')

    // Assert
    expect(wrapper.emitted('save')).to.be.undefined
    expect(wrapper.vm.$data.edited).to.equal(wrapper.props().value)
    expectClosed()
  })

  it('emits save when clicking done', () => {
    // Assume
    openDialog()

    // Act
    done.trigger('click')

    // Assert
    expect(wrapper.emitted('save'), '"save" event not emitted').to.exist
    expect(wrapper.emitted('save')[0], '"save" event does not have correct data').to.deep.equal(['Test'])
    expectClosed()
  })

  it('edits value then saves', () => {
    // Assume
    openDialog()
    let text = 'New Test'

    // Act
    textbox.setValue(text)

    done.trigger('click')

    // Assert
    expect(wrapper.emitted('save'), '"save" event not emitted').to.exist
    expect(wrapper.emitted('save')[0], '"save" event does not have correct data').to.deep.equal([text])
    expectClosed()
  })

  it('passes the name prop to the label', () => {
    // Act
    openDialog()

    // Assert
    expect(label.text(), 'Label is not equal to the "name" prop').to.equal(wrapper.props().name)
  })

  it.skip('tests validation', () => {
    // Assume
    openDialog()
    expect(wrapper.vm.$data.valid).to.be.true
    console.log('\n\n')
    console.log(wrapper.vm.$data.valid)
    console.log('\n\n')

    // Act
    textbox.element.value = ''
    textbox.trigger('input')
    textbox.trigger('update:error')

    console.log('\n\n')
    console.log(wrapper.vm.$data.valid)
    console.log('\n\n')
    // Assert
    expect(wrapper.vm.$data.valid).to.be.false
  })
})

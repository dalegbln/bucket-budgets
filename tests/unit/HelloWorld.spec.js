import { expect } from 'chai'
import {
  // mount,
  shallowMount,
  createLocalVue
} from '@vue/test-utils'
import WelcomePage from '@/pages/WelcomePage.vue'

import Vuetify from 'vuetify'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuetify)
localVue.use(Vuex)

describe('WelcomePage.vue', () => {
  let getters
  let store

  beforeEach(() => {
    getters = { title: () => 'Title' }

    store = new Vuex.Store({ getters })
  })

  it('renders msg as title', () => {
    const wrapper = shallowMount(WelcomePage, {
      // propsData: { title: msg },
      localVue,
      store,
      stubs: [
        'v-card',
        'v-card-title',
        'v-card-text',
        'v-card-action',
        'v-btn',
        'v-icon',
        'v-list',
        'v-list-tile',
        'v-list-tile-title',
        'v-list-tile-content',
        'v-list-tile-action'
      ]
    })
    expect(wrapper.text()).to.include(getters.title())
  })
})

import { expect } from 'chai'
import {
  mount,
  createLocalVue
} from '@vue/test-utils'

import UserManagementComponent from '@/components/user/UserManagementComponent'

import Vuetify from 'vuetify'
import Vuex from 'vuex'
// import Faker from 'faker'

describe('UserProfileComponent.vue', () => {
  let getters
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()

    localVue.use(Vuetify)
    localVue.use(Vuex)

    localVue.config.silent = true

    getters = { }
    store = new Vuex.Store({ getters })
  })

  it('mounts the component', () => {
    let wrapper = mount(UserManagementComponent, {
      localVue,
      store
    })

    expect(wrapper.exists()).to.be.true
  })
})

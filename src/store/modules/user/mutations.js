import {
  USER_VIEW,
  USER_LOADING,
  USER_INDEX,
  USER_SUCCESS,
  USER_ERROR
} from '@/store/actions/user'
import { AUTH_LOGOUT } from '@/store/actions/auth'

export default {
  [USER_VIEW]: (state, user) => {
    let u = state.users.findIndex(u => u.id === user.id)
    if (u && user) {
      state.users[u] = user
    } else {
      state.users.push(user)
    }
  },
  [USER_LOADING]: (state) => {
    state.status = 'loading'
  },
  [USER_INDEX]: (state, users) => {
    state.users = users
  },
  [USER_SUCCESS]: (state, response) => {
    state.status = 'success'
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.status = ''
    state.users = []
  }
}

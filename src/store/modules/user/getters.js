export default {
  usersLoaded: state => !!state.users.length,
  users: state => state.users,
  user: state => id => state.users.find(user => user.id === id)
}

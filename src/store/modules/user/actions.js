import {
  USER_VIEW,
  USER_INDEX,
  USER_LOADING,
  USER_SUCCESS,
  USER_ERROR,
  USER_CREATE,
  USER_UPDATE,
  USER_DELETE,
  USER_PASSWORD_RESET
} from '@/store/actions/user'
import { AUTH_PROFILE } from '@/store/actions/auth'

import api from '@/api'

export default {
  [USER_VIEW] ({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit(USER_LOADING)
      api.user.view(id)
        .then(response => {
          commit(USER_VIEW, response)
          commit(USER_SUCCESS)
          resolve(response)
        })
        .catch(reason => {
          commit(USER_ERROR)
          reject(reason)
        })
    })
  },
  [USER_INDEX] ({ commit }) {
    return new Promise((resolve, reject) => {
      api.user.index()
        .then(response => {
          commit(USER_INDEX, response)
          resolve()
        })
        .catch(reason => { reject(reason) })
    })
  },
  [USER_CREATE] ({ dispatch }, user) {
    return new Promise((resolve, reject) => {
      api.user.create(user)
        .then(response => {
          dispatch(USER_INDEX)
            .then(response => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => { reject(reason) })
    })
  },
  [USER_UPDATE] ({
    commit,
    dispatch,
    getters
  }, user) {
    commit(USER_LOADING)
    return new Promise((resolve, reject) => {
      api.user.update(user)
        .then(response => {
          let method = (getters.profile.id === user.id) ? AUTH_PROFILE : USER_INDEX
          dispatch(method)
            .then(() => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => {
          commit(USER_ERROR)
          reject(reason)
        })
    })
  },
  [USER_DELETE] ({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      api.user.delete(id)
        .then(response => {
          dispatch(USER_INDEX)
            .then(() => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => { reject(reason) })
    })
  },
  [USER_PASSWORD_RESET] (store, passwords) {
    return new Promise((resolve, reject) => {
      api.user.passwordReset(passwords.id, passwords)
        .then(response => {
          resolve(response)
        })
        .catch(reason => { reject(reason) })
    })
  }
}

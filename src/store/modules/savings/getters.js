export default {
  savings: state => state.items,
  savingsItem: state => id => state.items.find(item => item.id === id),
  savingsIndex: state => state.index,
  savingsPercent: state => state.items.reduce((total, item) => {
    return total + item.value
  }, 0)
}

import { SavingsItem } from '@/models/item'

import { LEDGER_SET_DIRTY } from '@/store/actions/ledger'
import {
  SAVINGS_LOAD,
  SAVINGS_SAVE,
  SAVINGS_ADD_MULTIPLE,
  SAVINGS_INCREMENT_INDEX,
  SAVINGS_CREATE,
  SAVINGS_UPDATE,
  SAVINGS_DELETE,
  SAVINGS_BALANCE
} from '@/store/actions/savings'

export default {
  [SAVINGS_LOAD] ({
    commit, getters, dispatch
  }, jsonString) {
    return new Promise((resolve, reject) => {
      let savings = JSON.parse(jsonString)
        .map(savings => {
          commit(SAVINGS_INCREMENT_INDEX)
          return new SavingsItem({
            id: getters.savingsIndex, ...savings
          })
        })

      dispatch(SAVINGS_ADD_MULTIPLE, savings).then(() => {
        resolve()
      })
    })
  },
  [SAVINGS_SAVE] ({ getters }) {
    return new Promise((resolve, reject) => {
      try {
        resolve(
          JSON.stringify(
            getters.savings
              .map(item => item.save())
          )
        )
      } catch (error) {
        reject(error)
      }
    })
  },
  [SAVINGS_ADD_MULTIPLE] ({
    commit, dispatch
  }, items) {
    return new Promise((resolve, reject) => {
      for (let i = 0, l = items.length; i < l; i++) {
        commit(SAVINGS_CREATE, items[i])
      }
      dispatch(SAVINGS_BALANCE).then(() => {
        console.log('Balanced')
        resolve()
      })
    })
  },
  [SAVINGS_CREATE] ({
    commit,
    getters, dispatch
  }, item) {
    return new Promise((resolve, reject) => {
      commit(SAVINGS_INCREMENT_INDEX)
      let savingsItem = new SavingsItem({
        id: getters.savingsIndex, ...item
      })
      commit(SAVINGS_CREATE, savingsItem)
      commit(LEDGER_SET_DIRTY)
      dispatch(SAVINGS_BALANCE, savingsItem.id).then(() => {
        resolve()
      })
    })
  },
  [SAVINGS_BALANCE] ({
    commit,
    getters
  }, ignoreId) {
    return new Promise((resolve, reject) => {
      if (getters.savings.length > 1) {
        /**
           * We are assuming that the item with ignoreId should not be included
           * in the calculations because its value has been set by the user.
           *
           * a = current total - (ignoreId.value || 0)
           * b = current value of item
           * x = change to apply to item
           * y = total change = z - a
           * z = new total = 100 - (ignoreId.value || 0)
           *
           * x = (b / a) * (a + y) - b
           */
        let a = (ignoreId) ? getters.savingsPercent - getters.savingsItem(ignoreId).value : getters.savingsPercent
        let z = (ignoreId) ? 100 - getters.savingsItem(ignoreId).value : 100
        let y = z - a
        if (Math.abs(y) > 0.1) {
          console.log(`a: ${a}, z: ${z}, y: ${y}`)
          for (let i = 0, l = getters.savings.length; i < l; i++) {
            let item = getters.savings[i]
            if (item.id !== ignoreId) {
              let x = Math.floor((item.value / a * (a + y) - item.value) * 1000) / 1000
              let n = item.value + x
              commit(SAVINGS_UPDATE, {
                id: item.id,
                value: (n >= 0.001) ? n : 0
              })
            }
          }

          /** There will probably be a difference of +- 0.001 so we will fix it by
         * adding the diff to the first item that does not have the ignoreId
         **/
          let m = Math.floor((100 - getters.savingsPercent) * 1000) / 1000
          if (Math.abs(m) < 0.001) {
            m = 0
          }
          let item = getters.savings.find(item => {
            return item.id !== ignoreId && item.value + m >= 0 && item.value + m <= 100
          })
          if (!item) {
            item = (getters.savings[0].id !== ignoreId) ? getters.savings[0] : getters.savings[1]
          }
          let n = item.value + m
          commit(SAVINGS_UPDATE, {
            id: item.id,
            value: (n >= 0.001) ? n : 0
          })
        }
      } else {
        if (getters.savings[0]) {
          commit(SAVINGS_UPDATE, {
            id: getters.savings[0].id,
            value: 100
          })
        }
      }
      resolve()
    })
  },
  [SAVINGS_UPDATE] ({
    commit, dispatch
  }, savingsItem) {
    return new Promise((resolve, reject) => {
      commit(SAVINGS_UPDATE, savingsItem)
      commit(LEDGER_SET_DIRTY)
      dispatch(SAVINGS_BALANCE, savingsItem.id).then(() => {
        resolve()
      })
    })
  },
  [SAVINGS_DELETE] ({
    commit, dispatch
  }, id) {
    return new Promise((resolve, reject) => {
      commit(SAVINGS_DELETE, id)
      commit(LEDGER_SET_DIRTY)
      dispatch(SAVINGS_BALANCE).then(() => {
        resolve()
      })
    })
  }

}

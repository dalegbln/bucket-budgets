import { LEDGER_SET_ACTIVE } from '@/store/actions/ledger'
import {
  SAVINGS_INCREMENT_INDEX,
  SAVINGS_CREATE,
  SAVINGS_UPDATE,
  SAVINGS_DELETE
} from '@/store/actions/savings'
import { AUTH_LOGOUT } from '@/store/actions/auth'

export default {
  [AUTH_LOGOUT] (state) {
    state.items = []
    state.index = 0
  },
  [LEDGER_SET_ACTIVE] (state) {
    state.items = []
  },
  [SAVINGS_CREATE] (state, item) {
    state.items.push(item)
  },
  [SAVINGS_INCREMENT_INDEX] (state) {
    state.index++
  },
  [SAVINGS_UPDATE] (state, savingsItem) {
    let item = state.items.find(item => item.id === savingsItem.id)
    item.update(savingsItem)
  },
  [SAVINGS_DELETE] (state, id) {
    let index = state.items.findIndex(item => item.id === id)
    state.items.splice(index, 1)
  }
}

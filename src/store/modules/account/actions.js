import api from '@/api'
import {
  ACCOUNT_INDEX,
  ACCOUNT_VIEW,
  ACCOUNT_CREATE,
  ACCOUNT_UPDATE,
  ACCOUNT_DELETE,
  ACCOUNT_SET_ACTIVE
} from '@/store/actions/account'
import { USER_VIEW } from '@/store/actions/user'

import {
  LEDGERS_REQUEST, LEDGER_SET_ACTIVE
} from '@/store/actions/ledger'

export default {
  [ACCOUNT_INDEX] ({
    commit,
    dispatch
  }) {
    return new Promise((resolve, reject) => {
      api.account.index()
        .then(response => {
          for (let i = 0, l = response.length; i < l; i++) {
            for (let j = 0, m = response[i].users.length; j < m; j++) {
              dispatch(USER_VIEW, response[i].users[j].id)
                .catch(reason => reject(reason))
            }
          }
          commit(ACCOUNT_INDEX, response)
          resolve(response)
        })
        .catch(error => reject(error))
    })
  },
  [ACCOUNT_VIEW] ({ commit }, id) {
    return new Promise((resolve, reject) => {
      api.account.view(id)
        .then(response => {
          commit(ACCOUNT_UPDATE, response)
          resolve(response)
        })
        .catch(error => reject(error))
    })
  },
  [ACCOUNT_CREATE] ({ dispatch }, account) {
    return new Promise((resolve, reject) => {
      api.account.create(account)
        .then(response => {
          dispatch(ACCOUNT_INDEX)
            .then(response => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => { reject(reason) })
    })
  },
  [ACCOUNT_UPDATE] ({ dispatch }, account) {
    return new Promise((resolve, reject) => {
      api.account.update(account)
        .then(response => {
          dispatch(ACCOUNT_INDEX)
            .then(() => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => {
          reject(reason)
        })
    })
  },
  [ACCOUNT_DELETE] ({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      api.account.delete(id)
        .then(response => {
          dispatch(ACCOUNT_INDEX)
            .then(() => {
              resolve(response)
            })
            .catch(reason => { reject(reason) })
        })
        .catch(reason => { reject(reason) })
    })
  },
  [ACCOUNT_SET_ACTIVE] ({
    commit, dispatch, getters
  }, id) {
    return new Promise((resolve, reject) => {
      let i = parseInt(id)
      commit(ACCOUNT_SET_ACTIVE, i)
      dispatch(LEDGERS_REQUEST)
        .then(() => {
          let date
          if (getters.newestLedger.date) {
            date = getters.newestLedger.date
          } else {
            let date = new Date()
            let y = date.getFullYear()
            let m = date.getMonth() + 1
            date = `${y}-${(m < 10) ? '0' : ''}${m}`
          }
          dispatch(LEDGER_SET_ACTIVE, date)
            .then(() => resolve())
            .catch(reason => reject(reason))
        })
        .catch(reason => reject(reason))
    })
  }
}

import {
  ACCOUNT_INDEX,
  ACCOUNT_UPDATE,
  ACCOUNT_SET_ACTIVE
} from '@/store/actions/account'
import { AUTH_LOGOUT } from '@/store/actions/auth'

export default {
  [ACCOUNT_INDEX] (state, data) {
    state.accounts = data
  },
  [ACCOUNT_UPDATE] (state, data) {
    let i = state.accounts.findIndex(account => {
      return account.id === data.id
    })
    state.accounts[i] = data
  },
  [ACCOUNT_SET_ACTIVE] (state, id) {
    id = parseInt(id)
    let account = state.accounts.find(account => {
      return account.id === id
    })
    state.activeAccount = account
  },
  [AUTH_LOGOUT] (state) {
    state.accounts = []
    state.activeAccount = {}
  }
}

export default {
  accounts: state => state.accounts,
  accountsLoaded: state => !!state.accounts.length,
  account: state => state.activeAccount,
  accountUsers: (state, getters) => id => {
    let a = state.accounts.find(account => account.id === id)
    if (a) {
      let users = []
      for (let i = 0, l = a.users.length; i < l; i++) {
        users.push(getters.user(a.users[i]))
      }
      return users.sort((a, b) => a.name - b.name)
    }
  },
  accountCoaches: (state, getters) => id => {
    return getters.accountUsers(id).filter(user => user.level < 2)
  }
}

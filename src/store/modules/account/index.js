import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  accounts: [],
  activeAccount: {}
}

export default {
  state,
  getters,
  actions,
  mutations
  // modules: {}
}

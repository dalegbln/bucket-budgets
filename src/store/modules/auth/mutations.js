import {
  AUTH_REQUEST,
  AUTH_SUCCESS,
  AUTH_LOGOUT,
  AUTH_ERROR,
  AUTH_PROFILE
} from '@/store/actions/auth'

export default {
  [AUTH_REQUEST] (state) {
    state.status = 'loading'
  },
  [AUTH_SUCCESS] (state, token) {
    state.status = 'success'
    state.token = token
  },
  [AUTH_ERROR] (state) {
    state.status = 'error'
  },
  [AUTH_LOGOUT] (state) {
    state.token = ''
    state.profile = {}
    state.status = ''
  },
  [AUTH_PROFILE] (state, profile) {
    state.profile = profile
  }
}

import api from '@/api'
import {
  AUTH_REQUEST,
  AUTH_SUCCESS,
  AUTH_LOGOUT,
  AUTH_ERROR,
  AUTH_PROFILE
} from '@/store/actions/auth'
import { APP_LOAD } from '@/store/actions'

export default {
  [AUTH_REQUEST] ({
    commit,
    dispatch
  }, credentials) {
    commit(AUTH_REQUEST)
    return new Promise((resolve, reject) => {
      api.auth.authenticate(credentials)
        .then(response => {
          const token = response
          console.log(token)
          localStorage.setItem('user-token', token)
          api.auth.registerToken(token)
          commit(AUTH_SUCCESS, token)
          dispatch(APP_LOAD)
            .then(() => {
              resolve()
            })
            .catch(reason => {
              reject(reason)
            })
        })
        .catch(reason => {
          commit(AUTH_ERROR)
          dispatch(AUTH_LOGOUT)
          reject(reason)
        })
    })
  },
  [AUTH_PROFILE] ({
    commit,
    getters
  }) {
    return new Promise((resolve, reject) => {
      if (!getters.logged) {
        reject(new Error('Not Authorized.'))
      }
      api.auth.profile()
        .then(response => {
          commit(AUTH_PROFILE, response)
          resolve(response)
        })
        .catch(reason => { reject(reason) })
    })
  },
  [AUTH_LOGOUT] ({ commit }) {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      localStorage.removeItem('user-token')
      resolve()
    })
  }
}

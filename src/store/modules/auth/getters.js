export default {
  logged: state => !!state.token,
  isProfileLoaded: state => !!state.profile.name,
  profile: state => state.profile,
  status: state => state.status,
  token: state => state.token
}

import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  status: '',
  profile: {},
  token: localStorage.getItem('user-token') || ''
}

export default {
  state,
  getters,
  actions,
  mutations
//   modules: {}
}

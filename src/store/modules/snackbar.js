import {
  SNACKBAR_SHOW,
  SNACKBAR_ERROR,
  SNACKBAR_RESET,
  SNACKBAR_SET_COLOR,
  SNACKBAR_SET_TEXT,
  SNACKBAR_SET_TIMEOUT,
  SNACKBAR_CLEAR_RESET
} from '../actions/snackbar'
import { AUTH_LOGOUT } from '@/store/actions/auth'

const state = {
  color: 'info',
  text: '',
  timeout: 6000,
  show: false,
  resets: 0
}

const getters = {
  snackbarShow: state => state.show,
  snackbarColor: state => state.color,
  snackbarText: state => state.text,
  snackbarTimeout: state => state.timeout
}
const actions = {
  [SNACKBAR_ERROR] ({
    commit,
    dispatch
  }, msg) {
    commit(SNACKBAR_SET_COLOR, 'error')
    commit(SNACKBAR_SET_TEXT, msg)
    dispatch(SNACKBAR_SHOW)
  },
  [SNACKBAR_SHOW] ({
    commit,
    state
  }) {
    /**
     * When a snackbar is shown, set a timeout to reset state.show to false.
     * Otherwise, we will not be able to reuse the snackbar
     * If multiple snackbars are created, only the last will be shown.
     * We increment state.resets so that only the last timeout will actually reset state.show
     */
    commit(SNACKBAR_SHOW, true)
    commit(SNACKBAR_RESET)
    setTimeout(() => {
      if (state.resets === 1) {
        commit(SNACKBAR_SHOW, false)
      }
      commit(SNACKBAR_CLEAR_RESET)
    }, state.timeout)
  }
}
const mutations = {
  [AUTH_LOGOUT] (state) {
    state.color = 'info'
    state.text = ''
    state.timeout = 6000
    state.show = false
    state.resets = 0
  },
  [SNACKBAR_SHOW] (state, show) {
    state.show = show
  },
  [SNACKBAR_RESET] (state) {
    state.resets++
  },
  [SNACKBAR_CLEAR_RESET] (state) {
    state.resets--
  },
  [SNACKBAR_SET_COLOR] (state, color) {
    state.color = color
  },
  [SNACKBAR_SET_TEXT] (state, text) {
    state.text = text
  },
  [SNACKBAR_SET_TIMEOUT]  (state, timeout) {
    state.timeout = timeout
  }
}

export default {
  state,
  actions,
  getters,
  mutations,
  modules: {}
}

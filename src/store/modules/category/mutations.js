import {
  CATEGORY_INCREMENT_INDEX,
  CATEGORY_CREATE,
  CATEGORY_UPDATE,
  CATEGORY_DELETE,
  ITEM_CREATE,
  ITEM_UPDATE,
  ITEM_DELETE,
  TRANSACTION_CREATE,
  TRANSACTION_UPDATE,
  TRANSACTION_DELETE
} from '@/store/actions/category'
import { LEDGER_SET_ACTIVE } from '@/store/actions/ledger'
import { AUTH_LOGOUT } from '@/store/actions/auth'

export default {
  [AUTH_LOGOUT] (state) {
    state.categories = []
    state.index = 0
  },
  [LEDGER_SET_ACTIVE] (state) {
    state.categories = []
  },
  [CATEGORY_CREATE] (state, item) {
    state.categories.push(item)
  },
  [CATEGORY_INCREMENT_INDEX] (state) {
    state.index++
  },
  [CATEGORY_UPDATE] (state, category) {
    state.categories.find(cat => cat.id === category.id).update(category)
  },
  [CATEGORY_DELETE] (state, id) {
    let index = state.categories.findIndex(cat => cat.id === id)
    state.categories.splice(index, 1)
  },
  [ITEM_CREATE] (state, {
    id, item
  }) {
    state.categories.find(cat => cat.id === id).createItem(item)
  },
  [ITEM_UPDATE] (state, {
    id, item
  }) {
    let i = state.categories.find(cat => cat.id === id).item(item.id)
    i.name = item.name
    i.targetValue = item.targetValue
  },
  [ITEM_DELETE] (state, {
    id, item
  }) {
    state.categories
      .find(cat => cat.id === id)
      .deleteItem(item.id)
  },
  [TRANSACTION_CREATE] (state, {
    id,
    itemId,
    transaction
  }) {
    state.categories
      .find(cat => cat.id === id)
      .item(itemId)
      .createTransaction(transaction)
  },
  [TRANSACTION_UPDATE] (state, {
    id,
    itemId,
    transaction
  }) {
    state.categories
      .find(cat => cat.id === id)
      .item(itemId)
      .transaction(transaction.id)
      .update(transaction)
  },
  [TRANSACTION_DELETE] (state, {
    id,
    itemId,
    transaction
  }) {
    state.categories
      .find(cat => cat.id === id)
      .item(itemId)
      .deleteTransaction(transaction.id)
  }
}

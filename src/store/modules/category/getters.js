export default {
  categories: state => state.categories,
  categoryIndex: state => state.index,
  category: state => id => state.categories.find(category => category.id === id),
  totalValue: state => state.categories.reduce((total, item) => {
    return (item.type === 'income') ? total + item.value : total - item.value
  }, 0),
  totalExpenses: state => state.categories.reduce((total, item) => {
    return (item.type === 'expense') ? total + item.value : total
  }, 0),
  totalIncome: state => state.categories.reduce((total, item) => {
    return (item.type === 'income') ? total + item.value : total
  }, 0)
}

import { CategoryItem } from '@/models/item'

import {
  CATEGORIES_LOAD,
  CATEGORIES_SAVE,
  CATEGORY_ADD_MULTIPLE,
  CATEGORY_INCREMENT_INDEX,
  CATEGORY_CREATE,
  CATEGORY_UPDATE,
  CATEGORY_DELETE,
  ITEM_CREATE,
  ITEM_UPDATE,
  ITEM_DELETE,
  TRANSACTION_CREATE,
  TRANSACTION_UPDATE,
  TRANSACTION_DELETE
} from '@/store/actions/category'
import { LEDGER_SET_DIRTY } from '@/store/actions/ledger'

export default {
  [CATEGORIES_LOAD] ({
    commit, getters, dispatch
  }, jsonString) {
    return new Promise((resolve, reject) => {
      let categories = JSON.parse(jsonString)
        .map(category => {
          commit(CATEGORY_INCREMENT_INDEX)
          return new CategoryItem({
            id: getters.categoryIndex, ...category
          })
        })

      dispatch(CATEGORY_ADD_MULTIPLE, categories).then(() => {
        resolve()
      })
    })
  },
  [CATEGORIES_SAVE] ({ getters }) {
    return new Promise((resolve, reject) => {
      try {
        resolve(
          JSON.stringify(
            getters.categories
              .map(category => category.save())
          )
        )
      } catch (error) {
        reject(error)
      }
    })
  },
  [CATEGORY_ADD_MULTIPLE] ({ commit }, items) {
    return new Promise((resolve, reject) => {
      for (let i = 0, l = items.length; i < l; i++) {
        commit(CATEGORY_CREATE, items[i])
      }
      resolve()
    })
  },
  [CATEGORY_CREATE] ({
    commit,
    getters
  }, {
    name,
    type
  }) {
    return new Promise((resolve, reject) => {
      commit(CATEGORY_INCREMENT_INDEX)
      let category = new CategoryItem({
        id: getters.categoryIndex, name: name, type: type
      })
      commit(CATEGORY_CREATE, category)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [CATEGORY_UPDATE] ({ commit }, category) {
    return new Promise((resolve, reject) => {
      commit(CATEGORY_UPDATE, category)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [CATEGORY_DELETE] ({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit(CATEGORY_DELETE, id)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [ITEM_CREATE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(ITEM_CREATE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [ITEM_UPDATE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(ITEM_UPDATE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [ITEM_DELETE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(ITEM_DELETE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [TRANSACTION_CREATE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(TRANSACTION_CREATE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [TRANSACTION_UPDATE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(TRANSACTION_UPDATE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  },
  [TRANSACTION_DELETE] ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(TRANSACTION_DELETE, payload)
      commit(LEDGER_SET_DIRTY)
      resolve()
    })
  }
}

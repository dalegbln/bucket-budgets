import {
  LEDGERS_SET_INDEX,
  LEDGERS_ADD,
  LEDGER_SET_ACTIVE,
  LEDGER_SET_DIRTY,
  LEDGER_SET_CLEAN,
  LEDGER_SET_STATUS,
  LEDGER_SET_VALUE,
  LEDGER_TEMPLATE_STORE,
  LEDGER_DIALOG_SET,
  LEDGER_DIALOG_SET_DATE
} from '../../actions/ledger'
import { AUTH_LOGOUT } from '@/store/actions/auth'

export default {
  [AUTH_LOGOUT] (state) {
    state.activeLedger = {}
    state.date = ''
    state.dialog = false
    state.isClean = true
    state.ledgers = []
    state.status = ''
    state.templates = []
  },
  [LEDGERS_SET_INDEX] (state, ledgers) {
    state.ledgers = ledgers
  },
  [LEDGERS_ADD] (state, stub) {
    state.ledgers.push(stub)
  },
  [LEDGER_SET_ACTIVE] (state, ledger) {
    state.activeLedger = ledger
  },
  [LEDGER_SET_DIRTY] (state) {
    state.isClean = false
  },
  [LEDGER_SET_CLEAN] (state) {
    state.isClean = true
  },
  [LEDGER_SET_STATUS] (state, status) {
    state.status = status
  },
  [LEDGER_SET_VALUE] (state, value) {
    state.activeLedger.value = parseFloat(value)
  },
  [LEDGER_TEMPLATE_STORE] (state, templates) {
    state.templates = templates
  },
  [LEDGER_DIALOG_SET] (state, value) {
    state.dialog = value
  },
  [LEDGER_DIALOG_SET_DATE] (state, date) {
    state.date = date
  }
}

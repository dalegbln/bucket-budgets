import {
  LEDGERS_REQUEST,
  LEDGER_REQUEST,
  LEDGERS_SET_INDEX,
  LEDGERS_ADD,
  LEDGER_SET_ACTIVE,
  LEDGER_SET_CLEAN,
  LEDGER_SET_DIRTY,
  LEDGER_SET_STATUS,
  LEDGER_SET_VALUE,
  LEDGER_CREATE,
  LEDGER_UPDATE,
  LEDGER_TEMPLATE_REQUEST,
  LEDGER_TEMPLATE_STORE,
  LEDGER_DIALOG_OPEN,
  LEDGER_DIALOG_CLOSE,
  LEDGER_DIALOG_SET,
  LEDGER_DIALOG_SET_DATE
} from '@/store/actions/ledger'
import {
  LOADING, SUCCESS, SAVING
} from '@/store/status_types'

import api from '@/api'
import {
  CATEGORIES_LOAD,
  CATEGORIES_SAVE
} from '@/store/actions/category'
import {
  SAVINGS_LOAD,
  SAVINGS_SAVE
} from '@/store/actions/savings'
import { LedgerItem } from '../../../models/item'

export default {
  [LEDGERS_REQUEST] ({
    commit, getters
  }) {
    return new Promise((resolve, reject) => {
      api.account.ledgers(getters.account.id)
        .then(response => {
          commit(LEDGERS_SET_INDEX, response)
          resolve()
        })
        .catch(error => reject(error))
    })
  },
  [LEDGER_REQUEST] ({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_SET_STATUS, LOADING)
      api.ledger.view(id).then(response => {
        commit(LEDGER_SET_STATUS, SUCCESS)
        resolve(response)
      }).catch(error => reject(error))
    })
  },
  [LEDGER_SET_ACTIVE] ({
    commit, dispatch, getters
  }, date) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_SET_STATUS, LOADING)
      let ledger = getters.ledgers.find(ledger => {
        return ledger.date === date
      })
      if (!ledger) {
        // dispatch(LEDGER_DIALOG_OPEN, date)
        // commit(LEDGER_SET_STATUS, SUCCESS)
        commit(LEDGER_DIALOG_SET_DATE, date)
        resolve({ openDialog: true })
      }
      dispatch(LEDGER_REQUEST, ledger.id)
        .then(response => {
          commit(LEDGER_SET_ACTIVE, response)
          commit(LEDGER_SET_CLEAN)
          Promise.all([
            dispatch(CATEGORIES_LOAD, response.categories),
            dispatch(SAVINGS_LOAD, response.savings)
          ])
            .then(() => {
              commit(LEDGER_SET_STATUS, SUCCESS)
              resolve()
            })
            .catch(reason => { reject(reason) })
        })
        .catch(error => reject(error))
    })
  },
  [LEDGER_SET_VALUE] ({ commit }, value) {
    return new Promise((resolve, reject) => {
      try {
        commit(LEDGER_SET_VALUE, value)
        commit(LEDGER_SET_DIRTY)
        resolve()
      } catch (error) {
        reject(error)
      }
    })
  },
  [LEDGER_CREATE] ({
    commit, dispatch, getters
  }, item) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_SET_STATUS, SAVING)
      api.ledger.create(getters.account.id, item)
        .then(response => {
          commit(LEDGERS_ADD, response)
          if (getters.ledger) {
            dispatch(LEDGER_UPDATE)
              .then(() => {
                dispatch(LEDGER_SET_ACTIVE, response.date)
                  .then(() => { resolve() })
                  .catch(reason => reject(reason))
              })
              .catch(reason => reject(reason))
          } else {
            dispatch(LEDGER_SET_ACTIVE, response.date)
              .then(response => { resolve(response) })
              .catch(reason => reject(reason))
          }
        })
    })
  },
  [LEDGER_UPDATE] ({
    commit, dispatch, getters
  }) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_SET_STATUS, SAVING)
      let savedLedger = new LedgerItem({
        id: getters.ledger.id,
        date: getters.ledger.date,
        value: getters.ledger.value
      })
      Promise.all([
        dispatch(CATEGORIES_SAVE),
        dispatch(SAVINGS_SAVE)
      ])
        .then(values => {
          savedLedger.categories = values[0]
          savedLedger.savings = values[1]

          api.ledger.update(savedLedger)
            .then(response => {
              if (response === savedLedger.id) {
                commit(LEDGER_SET_CLEAN)
                commit(LEDGER_SET_STATUS, SUCCESS)

                resolve()
              } else {
                reject(new Error('Failed to save ledger.'))
              }
            })
            .catch(error => reject(error))
        })
        .catch(reason => { reject(reason) })
    })
  },
  [LEDGER_TEMPLATE_REQUEST] ({
    commit, getters
  }) {
    return new Promise((resolve, reject) => {
      if (!getters.ledgerTemplates || getters.ledgerTemplates.length === 0) {
        api.ledger.templates()
          .then(response => {
            commit(LEDGER_TEMPLATE_STORE, response)
            resolve(getters.ledgerTemplates)
          })
          .catch(reason => reject(reason))
      } else {
        resolve(getters.ledgerTemplates)
      }
    })
  },
  [LEDGER_DIALOG_OPEN] ({ commit }, date) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_DIALOG_SET_DATE, date)
      commit(LEDGER_DIALOG_SET, true)
      resolve()
    })
  },
  [LEDGER_DIALOG_CLOSE] ({ commit }) {
    return new Promise((resolve, reject) => {
      commit(LEDGER_DIALOG_SET, false)
      resolve()
    })
  }
}

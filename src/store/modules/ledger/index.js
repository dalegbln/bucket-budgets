import getters from './getters'
import actions from './actions'
import mutations from './mutations'

/**
 * @param {LedgerItem} activeLedger
 * @param {LedgerStub} ledgers
 */
const state = {
  activeLedger: {},
  date: '',
  dialog: false,
  isClean: true,
  ledgers: [],
  status: '',
  templates: []
}

export default {
  state,
  getters,
  actions,
  mutations
//   modules: {}
}

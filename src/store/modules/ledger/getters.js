export default {
  ledger: state => state.activeLedger,
  ledgers: state => state.ledgers,
  newestLedger: state => {
    state.ledgers.sort((a, b) => a.date < b.date)
    return state.ledgers[0] || {}
  },
  ledgerStatus: state => state.status,
  ledgerClean: state => state.isClean,
  ledgerTemplates: state => state.templates,
  ledgerDialog: state => state.dialog,
  ledgerDialogDate: state => state.date
}

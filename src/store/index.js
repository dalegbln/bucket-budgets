import Vuex from 'vuex'
import Vue from 'vue'

import auth from './modules/auth'
import user from './modules/user'
import account from './modules/account'
import snackbar from './modules/snackbar'
import ledger from './modules/ledger'
import category from './modules/category'
import savings from './modules/savings'

import { APP_LOAD } from './actions'
import { ACCOUNT_INDEX } from '@/store/actions/account'
import { AUTH_PROFILE } from '@/store/actions/auth'
import { USER_INDEX } from '@/store/actions/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    user,
    account,
    snackbar,
    ledger,
    category,
    savings
  },
  state: {
    title: 'Bucket Budgets',
    navigationDrawer: true
  },
  getters: {
    title: state => {
      return state.title
    },
    navigationDrawer: state => state.navigationDrawer
  },
  actions: {
    [APP_LOAD] ({ dispatch }) {
      return new Promise((resolve, reject) => {
        Promise.all([
          dispatch(ACCOUNT_INDEX),
          dispatch(AUTH_PROFILE)
        ])
          .then(response => {
            if (response[1].level <= 1) {
              dispatch(USER_INDEX)
                .then(() => {
                  resolve()
                })
                .catch(reason => reject(reason))
            }
          })
          .catch(reason => reject(reason))
      })
    }
  },
  mutations: { }
})

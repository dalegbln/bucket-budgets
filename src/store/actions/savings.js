export const SAVINGS_ADD_MULTIPLE = 'SAVINGS_ADD_MULTIPLE'
export const SAVINGS_INCREMENT_INDEX = 'SAVINGS_INCREMENT_INDEX'
export const SAVINGS_BALANCE = 'SAVINGS_BALANCE'

export const SAVINGS_LOAD = 'SAVINGS_LOAD'
export const SAVINGS_SAVE = 'SAVINGS_SAVE'

export const SAVINGS_CREATE = 'SAVINGS_CREATE'
export const SAVINGS_UPDATE = 'SAVINGS_UPDATE'
export const SAVINGS_DELETE = 'SAVINGS_DELETE'

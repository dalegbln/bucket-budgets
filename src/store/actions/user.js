export const USER_INDEX = 'USER_INDEX'
export const USER_SUCCESS = 'USER_SUCCESS'
export const USER_ERROR = 'USER_ERROR'
export const USER_LOADING = 'USER_LOADING'

export const USER_VIEW = 'USER_VIEW'
export const USER_CREATE = 'USER_CREATE'
export const USER_UPDATE = 'USER_UPDATE'
export const USER_DELETE = 'USER_DELETE'

export const USER_PASSWORD_RESET = 'USER_PASSWORD_RESET'

export const SNACKBAR_SHOW = 'SNACKBAR_SHOW'
export const SNACKBAR_RESET = 'SNACKBAR_RESET'
export const SNACKBAR_CLEAR_RESET = 'SNACKBAR_CLEAR_RESET'

export const SNACKBAR_SET_COLOR = 'SNACKBAR_SET_COLOR'
export const SNACKBAR_SET_TEXT = 'SNACKBAR_SET_TEXT'
export const SNACKBAR_SET_TIMEOUT = 'SNACKBAR_SET_TIMEOUT'

export const SNACKBAR_ERROR = 'SNACKBAR_ERROR'

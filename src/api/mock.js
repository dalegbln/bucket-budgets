import { SavingsItem } from '../models/item'
import CategoryItem from '../models/item/CategoryItem'
import TransactionItem from '../models/item/TransactionItem'
import BudgetItem from '../models/item/BudgetItem'
import LedgerTemplates from './mock/templates'

import Faker from 'faker'
import User from '../models/User'

function formattedDate (date, includeDay) {
  let y = date.getFullYear()
  let m = date.getMonth() + 1
  let d = date.getDate()
  return `${y}-${(m < 10) ? '0' : ''}${m}${(includeDay) ? ((d < 10) ? '-0' : '-') + d : ''}`
}
export default {
/**
 * Mock API
 */
  data: {
    init: false,
    accounts: [],
    ledgers: [],
    users: [],
    accountId: 0,
    ledgerId: 0,
    categoryId: 0,
    budgetItemId: 0,
    savingsId: 0,
    transactionId: 0,
    userId: 0
  },
  accountFactory () {
    let id = this.data.accountId
    this.data.accountId++

    let updatedAt = new Date()
    let createdAt = new Date()
    createdAt.setUTCMonth(createdAt.getUTCMonth() - 1)
    for (let i = 0, l = Math.ceil(Math.random() * 10); i < l; i++) {
      let item = this.ledgerFactory(id)
      this.data.ledgers.push(item)
    }
    let users = [ this.userFactory(1).id ]
    for (let i = 0, l = Math.ceil(Math.random() * 2); i < l; i++) {
      users.push(this.userFactory().id)
    }
    return {
      id: id,
      name: `Account: ${id}`,
      users,
      updatedAt: updatedAt.toISOString(),
      createdAt: createdAt.toISOString()
    }
  },
  ledgerFactory (accountId) {
    let id = this.data.ledgerId
    this.data.ledgerId++

    let d = new Date()
    d.setMonth(-this.data.ledgers.length)
    d = formattedDate(d)
    let categories = []
    for (let i = 0, l = Math.ceil(Math.random() * 10) + 4; i < l; i++) {
      let c = this.categoryFactory()
      categories.push(c.save())
    }
    let savings = []
    for (let i = 0, l = Math.ceil(Math.random() * 10); i < l; i++) {
      let c = this.savingsFactory()
      savings.push(c.save())
    }
    return {
      id: id,
      accountId: accountId,
      value: Math.floor(Math.random() * 10000),
      date: d,
      categories: JSON.stringify(categories),
      savings: JSON.stringify(savings)
    }
  },
  categoryFactory () {
    // let id = this.data.categoryId

    let items = []
    for (let i = 0, l = Math.floor(Math.random() * 4); i < l; i++) {
      let c = this.budgetItemFactory()
      items.push(c)
    }
    return new CategoryItem({
      // id: id,
      name: `Category: ${this.data.categoryId++}`,
      items: items,
      type: (Math.random() > 0.25) ? 'expense' : 'income'
    })
  },
  savingsFactory () {
    let id = this.data.savingsId
    this.data.savingsId++

    return new SavingsItem({
      id: id,
      name: `Savings Item: ${id}`,
      value: Math.floor(Math.random() * 1000) / 10
    })
  },
  budgetItemFactory () {
    let id = this.data.budgetItemId
    this.data.budgetItemId++

    let items = []
    for (let i = 0, l = Math.floor(Math.random() * 4); i < l; i++) {
      let c = this.transactionFactory()
      items.push(c)
    }
    return new BudgetItem({
      // id: id,
      name: `Item: ${id}`,
      targetValue: Math.floor(Math.random() * 10000) / 100,
      transactions: items
    })
  },
  transactionFactory () {
    let id = this.data.transactionId
    this.data.transactionId++

    return new TransactionItem({
      // id: id,
      name: `Transaction: ${id}`,
      value: Math.floor(Math.random() * 10000) / 100,
      date: formattedDate(new Date(), true)
    })
  },
  userFactory (level = 2) {
    let id = this.data.userId
    this.data.userId++

    let user = {
      id,
      name: Faker.name.findName(),
      email: Faker.internet.email(),
      level,
      goals: JSON.stringify({
        future: Faker.lorem.paragraph(),
        long: Faker.lorem.paragraph(),
        short: Faker.lorem.paragraph()
      })
    }

    this.data.users.push(user)

    return user
  },
  setup () {
    if (!this.data.init) {
      for (let i = 0, l = Math.ceil(Math.random() * 4); i < l; i++) {
        let a = this.accountFactory()
        this.data.accounts.push(a)
      }
      this.userFactory(0)
      this.userFactory(1)
      for (let i = 0, l = Math.ceil(Math.random() * 4); i < l; i++) {
        this.userFactory(Math.floor(Math.random() * 3) + 1)
      }
      this.data.init = true
    }
  },
  wait: (ms, response) =>
    new Promise(resolve => setTimeout(() => resolve(response), ms)),
  waitOrFail: (ms, response, error) =>
    new Promise((resolve, reject) =>
      setTimeout(() => {
        Math.random() > 0.5 ? resolve(response) : reject(error)
      }, ms)
    ),
  authenticate () {
    if (!this.data.user) {
      this.data.user = this.userFactory(0)
    }
    let data = {
      user: this.data.user,
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
    }
    return data
  },
  accountUsers (id) {
    let users = [ this.userFactory(1) ]
    for (let i = 0, l = Math.floor(Math.random() * 5); i < l; i++) {
      users.push(this.userFactory())
    }
    return { data: users }
  },
  accounts () {
    this.setup()
    return { data: this.data.accounts }
  },
  account (id) {
    this.setup()
    let a = this.data.accounts.find(account => account.id === id)
    return a
  },
  ledger (id) {
    this.setup()

    let l = this.data.ledgers.find(ledger => ledger.id === id)
    return { data: l }
  },
  createLedger (accountId, item) {
    let id = this.data.ledgerId
    this.data.ledgerId++
    let ledger = {
      id: id,
      accountId: accountId,
      value: item.value,
      date: item.date,
      categories: item.categories,
      savings: item.savings
    }

    this.data.ledgers.push(ledger)

    return {
      data: {
        id: ledger.id,
        date: ledger.date
      }
    }
  },
  updateLedger (updatedLedger) {
    let l = this.data.ledgers.find(ledger => ledger.id === updatedLedger.id)
    Object.assign(l, updatedLedger)
    console.log('Mock Saving Ledger:')
    console.log(l)
    return { data: l.id }
  },
  ledgers (accountId) {
    let ledgers = this.data.ledgers
      .filter(ledger => ledger.accountId === accountId)
      .map(ledger => {
        let l = this.ledger(ledger.id).data
        return {
          id: l.id,
          date: l.date
        }
      })
    return { data: ledgers }
  },
  ledgerTemplates () {
    return { data: LedgerTemplates }
  },
  listUsers () {
    return { data: this.data.users }
  },
  user (id) {
    this.setup()

    let u = this.data.users.find(user => user.id === id)
    return { data: u }
  },
  createUser (user) {
    let u = new User({
      id: this.data.userId,
      ...user
    })
    this.data.userId++

    this.data.users.push(u)
    return {}
  },
  updateUser (updatedUser) {
    let u = this.data.users.find(user => user.id === updatedUser.id)
    Object.assign(u, updatedUser)
    console.log('Mock Saving User:')
    console.log(u)
    return { data: u.id }
  },
  deleteUser (userId) {
    let id = this.data.users.findIndex(u => u.id === userId)
    this.data.users.splice(id, 1)
    return { data: 0 }
  },
  resetPassword (id, passwords) {
    console.log(`Resetting Password for User: ${id}\nNew: ${passwords.new} Confirm: ${passwords.confirmed}`)
    return { data: id }
  },
  createAccount (account) {
    let a = {
      id: this.data.accountId,
      ...account
    }
    this.data.accountId++
    console.log(this.data.accountId)
    this.data.accounts.push(a)
    return {}
  },
  updateAccount (updatedAccount) {
    let a = this.data.accounts.find(account => account.id === updatedAccount.id)
    Object.assign(a, updatedAccount)
    a.updatedAt = new Date().toISOString()

    console.log('Mock Saving Account:')
    console.log(a)
    return { data: a.id }
  },
  deleteAccount (accountId) {
    let index = this.data.accounts.findIndex(u => u.id === accountId)
    this.data.accounts.splice(index, 1)
    return { data: accountId }
  }
}

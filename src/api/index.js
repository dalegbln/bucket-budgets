import mock from './mock'
// import axios from 'axios'

import LedgerStub from '@/models/LedgerStub'
import LedgerTemplate from '@/models/LedgerTemplate'
import {
  AccountItem, LedgerItem
} from '@/models/item'

import User from '@/models/User'

export default {
  auth: {
    authenticate (credentials) {
      return new Promise((resolve, reject) => {
        mock.wait(100, { data: mock.authenticate().token })
        // axios.post('/auth', credentials)
          .then(response => {
          /**
           * response.data @type {JWT}
           */
            let token = response.data
            resolve(token)
          })
          .catch(reason => { reject(reason) })
      })
      // return axios.post('/login', credentials)
    },
    profile () {
      return new Promise((resolve, reject) => {
        mock.wait(2000, { data: mock.authenticate().user })
        // axios.get('/user/profile')
          .then(response => {
          /**
           * response.data @type {User}
           */
            let user = new User(response.data)
            resolve(user)
          })
          .catch(reason => { reject(reason) })
      })
    },
    registerToken (token) {
      // axios.defaults.headers.common['Authorization'] = token
    },
    removeToken () {
      // delete axios.defaults.headers.common['Authorization']
    }
  },
  user: {
    index () {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.listUsers())
        // axios.get('/user')
          .then(response => {
          /**
           * response.data @type {{ id, name, email, level }[]}
           * Note: Should not include User.goals
           */
            let users = response.data.map(user => new User(user))
            resolve(users)
          })
          .catch(reason => { reject(reason) })
      })
    },
    create (user) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.createUser(user))
        // axios.post('/user', user)
          .then(response => {
            /**
             * response.data @type {User.id}
             */
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    view (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.user(id))
        // axios.get('/user/' + id)
          .then(response => {
          /**
           * response.data @type {{ id, name, email, level }[]}
           * Note: Should not include User.goals
           */
            let user = new User(response.data)
            resolve(user)
          })
          .catch(reason => { reject(reason) })
      })
    },
    update (updatedUser) {
      return new Promise((resolve, reject) => {
        let user = updatedUser.save()
        mock.wait(100, mock.updateUser(user))
        // axios.put('/user/' + user.id, user)
          .then(response => {
            /**
             * response.data @type {User.id}
             */
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    delete (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.deleteUser(id))
        // axios.delete('/user/' + id)
          .then(response => {
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    passwordReset (id, passwords) {
      return new Promise((resolve, reject) => {
        mock.wait(2000, mock.resetPassword(id, passwords))
          .then(response => {
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    }
  },
  account: {
    /**
     *
     * @returns {Promise<AccountItem[]>}
     */
    index () {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.accounts())
        // axios.get('/account')
          .then(response => {
            /**
             * response.data @type {AccountItem[]}
             */
            let accounts = response.data.map(item => {
              let newItem = new AccountItem(item)
              return newItem
            })
            resolve(accounts)
          })
          .catch(reason => { reject(reason) })
      })
    },
    create (account) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.createAccount(account))
        // axios.post('/account', account)
          .then(response => {
            /**
             * response.data @type {AccountItem.id}
             */
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    /**
     *
     * @param {Integer} id
     * @returns {Promise<AccountItem>}
     */
    view (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.account(id))
        // axios.get('/account/' + id)
          .then(response => {
            /**
             * response.data @type {AccountItem}
             */

            let account = new AccountItem(response.data)
            resolve(account)
          })
          .catch(reason => { reject(reason) })
      })
    },
    update (updatedAccount) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.updateAccount(updatedAccount))
        // axios.put('/account/' + updatedAccount.id, updatedAccount)
          .then(response => {
            /**
             * response.data @type {AccountItem.id}
             */

            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    delete (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.deleteAccount(id))
        // axios.delete('/account/' + id)
          .then(response => {
            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    /**
     *
     * @param {Integer} id
     * @returns {Promise<LedgerStub[]>}
     */
    ledgers (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.ledgers(id))
        // axios.get('/account/' + id + '/ledgers')
          .then(response => {
            /**
             * response.data @type {LedgerStub[]}
             */
            let ledgers = response.data.map(item => {
              return new LedgerStub(item)
            })
            resolve(ledgers)
          })
          .catch(reason => { reject(reason) })
      })
    }
  },
  ledger: {
    /**
     *
     *
     * @param {Number} id
     * @returns {Promise<LedgerItem>}
     */
    view (id) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.ledger(id))
        // axios.get('/ledger/' + id)
          .then(response => {
            /**
             * response.data @type {LedgerItem}
             */

            resolve(new LedgerItem(response.data))
          })
          .catch(reason => reject(reason))
      })
    },
    /**
     *
     *
     * @param {Number} accountId
     * @param {LedgerItem} item
     * @returns {Promise<LedgerStub>}
     */
    create (accountId, item) {
      return new Promise((resolve, reject) => {
        console.log(item)
        // let payload = item.save()
        mock.wait(100, mock.createLedger(accountId, item))
        // axios.post('/account/' + accountId + '/ledger', item)
          .then(response => {
            /**
             * response.data @type {LedgerStub}
             */

            resolve(new LedgerStub(response.data))
          })
          .catch(reason => { reject(reason) })
      })
    },
    update (item) {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.updateLedger(item))
        // axios.put('/ledger/' + item.id, item)
          .then(response => {
            /**
             * response.data @type {LedgerItem.id}
             */

            resolve(response.data)
          })
          .catch(reason => { reject(reason) })
      })
    },
    templates () {
      return new Promise((resolve, reject) => {
        mock.wait(100, mock.ledgerTemplates())
        // axios.get('/ledger/templates')
          .then(response => {
            /**
             * response.data @type {LedgerTemplate[]}
             */

            resolve(response.data.map(template => {
              return new LedgerTemplate(template)
            }))
          })
          .catch(reason => { reject(reason) })
      })
    }
  }
}

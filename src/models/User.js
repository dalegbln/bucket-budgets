export default class User {
  constructor ({
    id,
    email,
    name,
    level,
    goals
  }) {
    this.id = id
    this.email = email
    this.name = name
    this.level = level

    if (goals) {
      switch (typeof goals) {
        case 'string':
          this.goals = JSON.parse(goals)
          break
        case 'object':
          this.goals = goals
          break
        default:
          break
      }
    }
  }
  save () {
    return {
      id: this.id,
      email: this.email,
      name: this.name,
      level: this.level,
      goals: JSON.stringify(this.goals)
    }
  }
}

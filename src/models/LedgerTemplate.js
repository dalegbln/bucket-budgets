import {
  CategoryItem,
  SavingsItem
} from '@/models/item'

function sortName (a, b) {
  return a.name - b.name
}

export default class LedgerTemplate {
  constructor ({
    name,
    categories,
    savings
  } = {}) {
    this.name = name || 'Blank Template'

    if (categories) {
      let temp = ''
      if (typeof categories === 'string') {
        temp = JSON.parse(categories)
      } else {
        temp = categories
      }
      this.categories = temp.map(category => {
        let newCategory = new CategoryItem(category)
        newCategory.items = newCategory.items.map(item => {
          let newItem = item
          newItem.transactions = undefined
          return newItem
        })
        return newCategory
      })
    } else {
      this.categories = []
    }

    if (savings) {
      let temp = ''
      if (typeof savings === 'string') {
        temp = JSON.parse(savings)
      } else {
        temp = savings
      }
      this.savings = temp.map(item => {
        return new SavingsItem(item)
      })
    } else {
      this.savings = []
    }
  }

  get income () {
    return this.categories
      .filter(item => item.type === 'income')
      .sort(sortName)
  }

  get expense () {
    return this.categories
      .filter(item => item.type === 'expense')
      .sort(sortName)
  }

  clone () {
    return {
      name: this.name,
      categories: JSON.stringify(this.categories),
      savings: JSON.stringify(this.savings)
    }
  }
}

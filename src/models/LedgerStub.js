export default class LedgerStub {
  constructor ({
    id,
    date
  }) {
    this.id = id || 0
    this.date = date || ''
  }
  get name () {
    let [year, month] = this.date.split('-')
    return `${month} / ${year}`
  }
}

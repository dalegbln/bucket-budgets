import Item from './BaseItem'

export default class SavingsItem extends Item {
  constructor ({
    id,
    name,
    value,
    color
  }) {
    super({
      id, name
    })
    this.color = color || this.randomColor()
    this.value = value || 0
  }
  randomColor () {
    let colors = [
      'red',
      'pink',
      'purple',
      'deep-purple',
      'indigo',
      'blue',
      'light-blue',
      'cyan',
      'teal',
      'green',
      'light-green',
      'lime',
      'yellow',
      'amber',
      'orange',
      'deep-orange',
      'brown',
      'blue-grey',
      'grey'
    ]
    let shades = ['', 'lighten', 'darken', 'accent']
    let value = Math.ceil(Math.random() * 4)
    let selection = colors[Math.floor(Math.random() * colors.length)]
    let shade = shades[Math.floor(Math.random() * shades.length)]
    if (shade) {
      selection += ' ' + shade + '-' + value
    }
    return selection
  }

  update ({
    name,
    value,
    color
  }) {
    if (name && name !== this.name) {
      this.name = name
    }
    if (value && value !== this.value) {
      this.value = value
    }
    if (color && color !== this.color) {
      this.color = color
    }
  }

  save () {
    return {
      name: this.name,
      value: this.value,
      color: this.color
    }
  }
}

import Item from './BaseItem'
import BudgetItem from './BudgetItem'

/**
 *
 *
 * @export
 * @class CategoryItem
 * @extends {Item}
 */
export default class CategoryItem extends Item {
  /**
   * Creates an instance of CategoryItem.
   * @param {any} {
   *     id,
   *     name,
   *     items  @type {BudgetItem[]}
   *   }
   * @memberof CategoryItem
   */
  constructor ({
    id,
    name,
    items,
    type
  }) {
    super({
      id,
      name
    })
    this.type = type
    let list = items || []
    this.itemIndex = 0
    this.items = list.map(item => {
      let i = (typeof item === 'string') ? JSON.parse(item) : item
      let budget = new BudgetItem({
        id: this.itemIndex++, ...i
      })
      return budget
    })
  }

  get value () {
    let initValue = 0
    let sum = this.items.reduce((total, item) => {
      return total + item.value
    }, initValue)
    return sum
  }

  get targetValue () {
    let initValue = 0
    let sum = this.items.reduce((total, item) => {
      return total + item.targetValue
    }, initValue)
    return sum
  }

  update ({
    name,
    type
  }) {
    if (name && name !== this.name) {
      this.name = name
    }
    if (type && type !== this.type) {
      this.type = type
    }
  }

  item (id) { return this.items.find(item => item.id === id) }

  createItem ({
    name,
    targetValue
  }) {
    this.items.push(new BudgetItem({
      id: this.itemIndex++, name, targetValue
    }))
  }

  deleteItem (id) {
    let index = this.items.findIndex(item => item.id === id)
    console.log(index)
    if (index >= 0) {
      this.items.splice(index, 1)
    }
    console.log(this.items)
  }

  save () {
    let items = []
    for (let i = 0, l = this.items.length; i < l; i++) {
      items.push(this.items[i].save())
    }
    return {
      name: this.name,
      type: this.type,
      items
    }
  }
}

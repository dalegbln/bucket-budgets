import Item from './BaseItem'
import TransactionItem from './TransactionItem'

/**
 *
 *
 * @export
 * @class BudgetItem
 * @extends {Item}
 */
export default class BudgetItem extends Item {
  /**
   * Creates an instance of BudgetItem.
   * @param {any} {
   *     id,
   *     name,
   *     value,
   *     targetValue, @type {number}
   *     transactions, @type {TransactionItem[]}
   *   }
   * @memberof BudgetItem
   */
  constructor ({
    id,
    name,
    targetValue,
    transactions
  }) {
    super({
      id,
      name
    })

    this.targetValue = targetValue
    let list = transactions || []
    this.transactionIndex = 0
    this.transactions = list.map(item => {
      let budget = new TransactionItem({
        id: this.transactionIndex++, ...item
      })
      return budget
    })
  }
  get value () {
    let initValue = 0
    let sum = this.transactions.reduce((total, item) => total + item.value, initValue)
    return sum
  }

  transaction (id) { return this.transactions.find(transaction => transaction.id === id) }

  createTransaction (transaction) {
    console.log(transaction)
    this.transactions.push(new TransactionItem({
      id: this.transactionIndex++, ...transaction
    }))
  }

  deleteTransaction (id) {
    let index = this.transactions.findIndex(transaction => transaction.id === id)
    console.log(index)
    if (index >= 0) {
      this.transactions.splice(index, 1)
    }
    console.log(this.transactions)
  }

  save () {
    let transactions = []
    for (let i = 0, l = this.transactions.length; i < l; i++) {
      transactions.push(this.transactions[i].save())
    }
    return {
      name: this.name,
      targetValue: this.targetValue,
      transactions
    }
  }
}

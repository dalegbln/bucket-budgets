/**
 * @abstract
 *
 * @export
 * @class Item
 */
export default class Item {
  /**
   * Creates an instance of Item.
   * @param {any} {
   *     id,
   *     name,
   *     value
   *   }
   * @memberof Item
   */
  constructor ({
    id,
    name,
    value
  }) {
    if (id !== undefined) {
      this.id = id
    }
    if (name !== undefined) {
      this.name = name
    }
    if (value !== undefined) {
      this.value = value
    }
  }
}

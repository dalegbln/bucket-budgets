import Item from './BaseItem'

/**
 * The Ledger Item
 *
 * @export
 * @class LedgerItem
 * @extends {Item}
 */
export default class LedgerItem extends Item {
  /**
   * Creates an instance of LedgerItem.
   * @param {any} {
   *     id,
   *     name,
   *     value,
   *     date, @type {string}
   *     categories, @type {string} JSON of @type {CategoryItem[]}
   *     savings @type {string} JSON of @type {SavingsItem[]}
   *   }
   * @memberof LedgerItem
   */
  constructor ({
    id,
    date,
    value,
    categories,
    savings
  }) {
    super({
      id,
      value
    })

    this.date = date || ''
    if (categories) {
      this.categories = categories
    } else {
      this.categories = ''
    }
    if (savings) {
      this.savings = savings
    } else {
      this.savings = ''
    }
    // this.savedValue = savedValue
  }

  get name () {
    return this.date
  }

  // get value () {
  //   return store.getters.categories.reduce((total, item) => {
  //     return (item.type === 'income') ? total + item.value : total - item.value
  //   }, 0)
  // }
}

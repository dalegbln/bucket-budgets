import Item from './BaseItem'

/**
 *
 *
 * @export
 * @class TransactionItem
 * @extends {Item}
 */
export default class TransactionItem extends Item {
/**
 * Creates an instance of TransactionItem.
 * @param {any} {
 *     id,
 *     name,
 *     value,
 *     date,
 *     receipt,
 *   }
 * @memberof TransactionItem
 */
  constructor ({
    id,
    name,
    value,
    date,
    receipt
  }) {
    super({
      id,
      name,
      value
    })

    this.date = date
    this.receipt = receipt
  }

  update ({
    name,
    value,
    date,
    receipt
  }) {
    if (name && name !== this.name) {
      this.name = name
    }
    if (value && value !== this.value) {
      this.value = value
    }
    if (date && date !== this.date) {
      this.date = date
    }
    if (receipt && receipt !== this.receipt) {
      this.receipt = receipt
    }
  }

  save () {
    return {
      name: this.name,
      value: this.value,
      date: this.date,
      receipt: this.receipt
    }
  }
}

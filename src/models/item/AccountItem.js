import Item from './BaseItem'

export default class AccountItem extends Item {
  constructor ({
    id,
    name,
    updatedAt,
    createdAt,
    users
  }) {
    super({
      id, name
    })
    this.updatedAt = (updatedAt) ? new Date(updatedAt) : new Date()
    this.createdAt = (createdAt) ? new Date(createdAt) : new Date()
    this.users = users || []
  }
}

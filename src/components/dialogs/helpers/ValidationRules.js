export const required = v => (v === 0 || !!v) || 'Required'
export const email = v => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'Please enter a valid email.'
export const password = v => (!!v && v.length >= 6) || 'Password have to be at least 6 characters long'
export const equals = value => v => v === value || 'Failed to confirm'

export const maxCharacters = length => v => v.length <= length || 'Input is too long!'
export const price = v => /^\d+(\.\d\d?)?$/.test(v) || 'Invalid price. (e.g. 100 | 100.1 | 100.11)'

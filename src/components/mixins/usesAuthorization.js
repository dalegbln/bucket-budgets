import * as userLevel from '@/components/helpers/UserLevelConstants'

export default {
  data () {
    return { userLevel }
  },
  methods: {
    isAuthorized (level) {
      return this.$store.getters.profile.level <= level || this.$store.getters.profile.level <= 0
    }
  }
}

import {
  SNACKBAR_SHOW,
  SNACKBAR_SET_COLOR,
  SNACKBAR_SET_TEXT
} from '@/store/actions/snackbar'
import { mapGetters } from 'vuex'

export default {
  computed: { ...mapGetters(['snackbarShow', 'snackbarColor', 'snackbarText', 'snackbarTimeout']) },
  methods: {
    showError (message) {
      this.showSnackbar(message, 'error')
    },
    showInfo (message) {
      this.showSnackbar(message, 'info')
    },
    showSuccess (message) {
      this.showSnackbar(message, 'success')
    },
    showWarning (message) {
      this.showSnackbar(message, 'warning')
    },
    showSnackbar (message, color) {
      this.$store.commit(SNACKBAR_SET_COLOR, color || 'error')
      this.$store.commit(SNACKBAR_SET_TEXT, message)
      this.$store.dispatch(SNACKBAR_SHOW)
    },
    closeSnackbar () {
      this.$store.commit(SNACKBAR_SHOW, false)
    }
  }
}

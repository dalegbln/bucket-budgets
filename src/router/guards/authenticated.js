import store from '@/store'

export default (to, from, next) => {
  if (store.getters.logged) {
    next()
    return
  }
  next('/')
}

import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/pages/WelcomePage'
import Account from '@/pages/AccountPage'
import Dashboard from '@/pages/DashboardPage'

import isAuthenticated from './guards/authenticated'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      name: 'Welcome',
      component: Welcome
    },
    // {
    //   path: '/home',
    //   name: 'Home',
    //   components: {
    //     default: User,
    //     navigation: AccountNavbar
    //   },
    //   beforeEnter: isAuthenticated
    // }
    {
      path: '/account/:id',
      component: Account,
      name: 'account',
      props: true,
      beforeEnter: isAuthenticated
    },
    {
      path: '/dashboard',
      component: Dashboard,
      name: 'dashboard',
      beforeEnter: isAuthenticated
    }
  ],
  mode: 'history'
})
